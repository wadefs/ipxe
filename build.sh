#!/bin/sh

echo "Choose ROM name :"
echo "  1: 10ec8168.rom"
echo "  2: 10ec1010.rom"
read -p "Enter your choice [1]: " choice
choice=${choice:-1}

if [ $choice = 1 ]; then
	FN="10ec8168"
else
	FN="10ec1010"
fi

cd src
make bin/$FN.rom EMBED=q8clnt.ipxe
mv ./bin/$FN.rom ../ROM/$FN.rom
cd ..
wine ./tools/mmtool_v5.exe
