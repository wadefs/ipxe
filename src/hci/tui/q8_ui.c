FILE_LICENCE ( GPL2_OR_LATER_OR_UBDL );

#include <stdio.h>
#include <stdarg.h>
#include <unistd.h>
#include <string.h>
#include <curses.h>
#include <stdlib.h>
#include <ipxe/console.h>
#include <ipxe/settings.h>
#include <ipxe/editbox.h>
#include <ipxe/keys.h>
#include <ipxe/ansicol.h>
#include <ipxe/jumpscroll.h>
#include <ipxe/settings_ui.h>
#include <ipxe/in.h>
#include <ipxe/crc32.h>
#include <ipxe/netdevice.h>
#include <config/branding.h>

#define MODLE_NAME						"E-01"


#define HATCH                           0xB0
#define DOUBLE_BAR                      0xCD
#define BOX                             0xFE
#define CHECK                           0xFB
#define TRIPLET                         0xF0
#define OMEGA                           0xEA
#define PI                              0xE3
#define UPARROW                         0x18
#define DOWNARROW                       0x19
#define RIGHTARROW                      0x1A
#define LEFTARROW                       0x1B
#define SINGLE_BAR                      0xC4
#define BACK_ARROWHEAD                  0x11
#define LRCORNER                        0xD9
#define DEGREE                          0xF8
#define PLUSMINUS                       0xF1

#define Q_WINDOW_LEFT_TOP               0xD5
#define Q_WINDOW_RIGHT_TOP              0xB8
#define Q_WINDOW_SIDE                   0xB3
#define Q_WINDOW_LEFT_BOTTOM            0xD4
#define Q_WINDOW_RIGHT_BOTTOM           0xBE
#define Q_WINDOW_LEFT_TEE               0xC6
#define Q_WINDOW_RIGHT_TEE              0xB5
#define Q_WINDOW_LEFT_TOP_DOUBLESIDE    0xD6
#define Q_WINDOW_RIGHT_TOP_DOUBLESIDE   0xB7

#define WRAP   0
#define NOWRAP 1

extern void _wputch ( WINDOW *win, chtype ch, int wrap ) __nonnull;
extern void _wputc  ( WINDOW *win, char c, int wrap ) __nonnull;

static uint32_t q8_ip;
static uint32_t q8_gw;
static uint32_t q8_srv0;
static uint32_t q8_srv1;


static struct q8_pwd {
	uint32_t prefix:6;
	uint32_t rsvd:2;
	uint32_t password:24;
} q8_nm_pwd;

char q8_pwd[8] = "";

extern size_t data_has_get;
/*
 * netmask helper function
 */
/* prefix to addr convert */

static uint32_t prefix_to_addr(uint32_t prefix)
{
	uint32_t mask = 0;

	if (prefix == 0)
		mask = ( ~((uint32_t) -1) );
	else
		mask = (~((1 << (32 - prefix)) - 1));

	return htonl(mask);
}


/* addr to prefix convert */

static uint32_t addr_to_prefix(uint32_t addr)
{
	uint32_t n = addr;
	int i = 0;

	while (n > 0) {
		if (n & 1)
			i++;
		n = n >> 1;
	}

	return i;
}

#if 0 /* test code */
int test_addr_prefix(void)
{
	for (uint32_t i = 0; i < 32; i ++) {
		struct in_addr in;
		uint32_t addr = in.s_addr = prefix_to_addr(i);
		printf( "%d -> %08x %s -> %d, %s\n",
			i, addr, inet_ntoa(in), addr_to_prefix(addr),
			i != addr_to_prefix(addr) ? "ERR" : "OK");
	}
}
#endif

static void _cmsg (WINDOW *win, unsigned int y, unsigned int x,
		  const char *fmt, va_list args )
{
	char buf[COLS-x];
	size_t len;

	len = vsnprintf (buf, sizeof ( buf ), fmt, args);
	x += (COLS - len) / 2;
	wmove(win, y, x);
	wprintw(win, "%s", buf);
}

static void cmsg (WINDOW *win, unsigned int y, unsigned int x
		  ,const char *fmt, ... )
{
	va_list args;

	va_start ( args, fmt );
	_cmsg ( win, y, x, fmt, args );
	va_end ( args );
}

/*
gui - 80x25
  0         1         2         3         4         5         6        7         8
  01234567890123456789012345678901234567890123456789012345678901234567801234567890
00+------------------------------------------------------------------------------+
 1|                                                                              | <--- title0
 2|                                                                              | <--- title1
 3+------------------------------------------------------------------------------+
 4|                                                                              |
 5|  xxxxxxxxxx                                                                  |
 6| ---------------                                                              |
 7|  (A)                                                                         |
 8|                                                                              |
 9|                                                                              |
 0|  (Q)                                                                         |
  |  (S)                                                                         |
  |                                                                              |
  |  Select ...                                                                  |
  |                                                                              |
  |                                                                              |
  |                                                                              |
  |                                                                              |
  +------------------------------------------------------------------------------+
*/
static void draw_root_window ( void )
{
	WINDOW *win = stdscr;

	wborder(win,
		win->attrs | Q_WINDOW_SIDE,
		win->attrs | Q_WINDOW_SIDE,
		win->attrs | DOUBLE_BAR,
		win->attrs | DOUBLE_BAR,
		win->attrs | Q_WINDOW_LEFT_TOP,
		win->attrs | Q_WINDOW_RIGHT_TOP,
		win->attrs | Q_WINDOW_LEFT_BOTTOM,
		win->attrs | Q_WINDOW_RIGHT_BOTTOM);

	unsigned int y = 0;
	y++; cmsg(win, y, 0, "Q8 Network Boot Loader v1.0");
	y++; cmsg(win, y, 0, "Copyright 2020 Arista Corporation");

	y++;
	wmove(win, y, 0);
	_wputch( win, Q_WINDOW_LEFT_TEE|win->attrs, NOWRAP );

	wmove(win, y, 1);
	whline(win,  win->attrs | DOUBLE_BAR, 80 - 2);

	wmove(win, y, win->width-1);
	_wputch( win, Q_WINDOW_RIGHT_TEE|win->attrs, NOWRAP );
}

#define Y_STS     5
#define X_STS     2
#define X_STS_END 79

static void clear_line(unsigned int y, unsigned int x)
{
	WINDOW *win = stdscr;

	wmove(win, Y_STS+y, x);
	for (; x < X_STS_END; x++) {
		_wputc(win, ' ', WRAP);
	}
	wmove(win, Y_STS+y, X_STS);
}

static void update_status(const char *fmt, ...)
{
	WINDOW *win = stdscr;

	clear_line(0, X_STS);

	va_list args;
	va_start ( args, fmt );
	vw_printw(win, fmt, args);
	va_end ( args );
}

static int bar_y = 0;

void update_progress(unsigned int sts)
{
	WINDOW *win = stdscr;

	unsigned int max_width = win->width - 4;
	int height = 4;
	int start_x = X_STS;
	int start_y = bar_y;

	static WINDOW *pwin = NULL;
	if (pwin == NULL)
		pwin = newwin(height, max_width, start_y, start_x);

	if (sts == 0) {
		wborder(pwin,
			win->attrs | Q_WINDOW_SIDE,
			win->attrs | Q_WINDOW_SIDE,
			win->attrs | DOUBLE_BAR,
			win->attrs | DOUBLE_BAR,
			win->attrs | Q_WINDOW_LEFT_TOP,
			win->attrs | Q_WINDOW_RIGHT_TOP,
			win->attrs | Q_WINDOW_LEFT_BOTTOM,
			win->attrs | Q_WINDOW_RIGHT_BOTTOM);
	}

	wmove(pwin, 2, 1);

	if (sts == 0) {
		sts = 100;
		wcolor_set(pwin, CPAIR_NORMAL, NULL);
	} else {
		wcolor_set(pwin, CPAIR_SEPARATOR, NULL);
	}
 
	unsigned int end = sts ? ((max_width-3) * sts)/100 : max_width;
	for(unsigned int i = 0; i <= end; i++){
		_wputch(pwin, 219 | pwin->attrs, NOWRAP);
	}
}

typedef enum {
	E_REDRAW = 1,
	E_TIMEOUT,
	E_KEY,
	E_NEXT,
	E_SHELL,

	E_ERROR = E_SHELL,
} event_t;

static uint32_t config_ip(WINDOW *win, int y, char *msg)
{
	int oldy = y - 1;

	const char prompt[] = "Enter 'D' for DHCP/PXE or Static IP as X.X.X.X :";
	y ++; wmove(win, Y_STS+y, X_STS); wprintw(win, "%s", msg);
	y ++; wmove(win, Y_STS+y, X_STS); wprintw(win, "%s", prompt);

	struct in_addr in;
	int done = 0;
	uint32_t ip = 0;
	struct edit_box ip_box;
	char buf[32] = {0};
	init_editbox(&ip_box, buf, sizeof(buf), win, Y_STS+y,
		     win->curs_x, 16, EDITBOX_NO_UNDERLINE);
	do {
		draw_editbox ( &ip_box );
		int key = getkey(0);
		switch (key) {
		case 'd':
		case 'D':
			done = 2;
			clear_line(y-4, X_STS); //clear (Q)....
			clear_line(y-5, X_STS);	//clear (S)....
			break;
		case '.':
		case '0' ... '9':
		case BACKSPACE:
			edit_editbox (&ip_box, key);
			break;
		case CR:
		case LF:
			if (inet_aton(buf, &in)) {
				ip = in.s_addr;
				done = 1;
			}
			break;
		}
	} while (done == 0);

	do {
		wmove(win, Y_STS+y, X_STS);
		for (int x = X_STS; x < X_STS_END; x++) {
			_wputc(win, ' ', WRAP);
		}
		y --;
	} while (y != oldy);

	return ip;
}

static void check_password(WINDOW *win, int y)
{
	char password[8] = {0};
	struct edit_box password_box;
	int done = 0;
	int key;

	wmove(win, Y_STS+y, X_STS); wprintw(win, "%s", "Enter password:");
	init_editbox(&password_box, password, sizeof(password), win, Y_STS+y,
		     win->curs_x, 8, EDITBOX_NO_UNDERLINE | EDITBOX_STARS );
	draw_editbox ( &password_box );
	
	do {
		key = getkey(0);
		switch (key) {
		default:
			edit_editbox (&password_box, key);
			break;
		case CR:
		case LF:
			if (strcmp(password, q8_pwd) == 0) 
				done = 1;
			else
			{
				curs_set(0);
				clear_line(y, X_STS);
				wprintw(win, "%s", "Enter password:");
				curs_set(1);
			}			
			break;
		}
		draw_editbox ( &password_box );
	} while (done == 0);
}


static uint32_t config_pwd(WINDOW *win, int y, char *msg)
{
	int oldy = y - 1;
	y ++; wmove(win, Y_STS+y, X_STS); wprintw(win, "%s", msg);

	int done = 0;
	struct edit_box password_box;
	struct edit_box vpassword_box;
	char password[8] = {0};
	char vpassword[8] = {0};
	int verfiy = 0;
	int move = 0;

	init_editbox(&password_box, password, sizeof(password), win, Y_STS+y,
		     win->curs_x, 8, EDITBOX_NO_UNDERLINE | EDITBOX_STARS );
	init_editbox(&vpassword_box, vpassword, sizeof(vpassword), win, Y_STS+y+1,
		     win->curs_x-2, 8, EDITBOX_NO_UNDERLINE | EDITBOX_STARS );
	do {
		if (verfiy == 1){
			if (move == 0){
				y ++; wmove(win, Y_STS+y, X_STS);
				wprintw(win, "%s", "Verify passsword:");
				move = 1;
			}
			draw_editbox ( &vpassword_box );
		} else {
			draw_editbox ( &password_box );
		}
		int key = getkey(0);
		switch (key) {
		default:
			if (verfiy == 1)
				edit_editbox (&vpassword_box, key);
			else
				edit_editbox (&password_box, key);
			break;
		case CR:
		case LF:
			if (verfiy == 0)
				verfiy = 1;
			else
				done = 1;
			break;
		}
	} while (done == 0);

	do {
		wmove(win, Y_STS+y, X_STS);
		for (int x = X_STS; x < X_STS_END; x++) {
			_wputc(win, ' ', WRAP);
		}
		y --;
	} while (y != oldy);

	if (strcmp(password, vpassword) == 0) {
		strcpy(q8_pwd, password);		
		return crc32_le(0x0, password, strlen(password));
	}

	return 0;
}

/************************************************************************/
#include <rtc.h>

enum {
	NVRAM_FIRST_BYTE   = 14,
	PC_CKS_RANGE_START = 2,
	PC_CKS_RANGE_END   = 31,
	NVRAM_BYTES = (128-NVRAM_FIRST_BYTE),
	PC_CKS_LOC  = 32,

	NV_DATA_OFF = 0x50,
};

static unsigned int rtc_readb ( int address )
{
	outb ( address, CMOS_ADDRESS );
	return inb ( CMOS_DATA );
}

static int rtc_is_busy ( void ) {
	return ( rtc_readb ( RTC_STATUS_A ) & RTC_STATUS_A_UPDATE_IN_PROGRESS );
}

static unsigned char nvram_readb (int i)
{
	while ( rtc_is_busy() ) {}
	return rtc_readb(i);
}

static void rtc_writeb ( int address, unsigned int data ) {
	outb ( address, CMOS_ADDRESS );
	outb ( data, CMOS_DATA );
}

static void nvram_writeb(unsigned char data, int i)
{
	while ( rtc_is_busy() ) {}
	rtc_writeb(i, data);
}

static struct conf_map {
	uint32_t *data;
	uint32_t addr;
} conf_addr[] = {
	{ .data = &q8_ip,   .addr = 0x50},
	{ .data = (uint32_t *)&q8_nm_pwd,   .addr = 0x5c},
	{ .data = &q8_gw,   .addr = 0x7c},
	{ .data = &q8_srv0, .addr = 0x54},
	{ .data = &q8_srv1, .addr = 0x78},
	{ .data = (uint32_t *)q8_pwd,  .addr = 0x70},
	{ .data = (uint32_t *)(q8_pwd+4),  .addr = 0x74},

	{ NULL },
};

static void config_load(void)
{
	struct conf_map *c = conf_addr;

	do {
		uint8_t *p = (uint8_t *)c->data;
		uint32_t addr = c->addr;

		p[0] = nvram_readb(addr);
		p[1] = nvram_readb(addr+1);
		p[2] = nvram_readb(addr+2);
		p[3] = nvram_readb(addr+3);

		c ++;
	} while (c->data);
}

static void config_save(void)
{
	struct conf_map *c = conf_addr;

	do {
		uint8_t *p = (uint8_t *)c->data;
		uint32_t addr = c->addr;

		nvram_writeb(p[0], addr);
		nvram_writeb(p[1], addr+1);
		nvram_writeb(p[2], addr+2);
		nvram_writeb(p[3], addr+3);

		c ++;
	} while (c->data);
}

/************************************************************************/
static event_t update_config_menu(void)
{
	const char menu[] = "IP configuration menu";
	update_status("%s", menu);

	int y = 1;

	WINDOW *win = stdscr;
	clear_line(y, X_STS);
	whline(win,  win->attrs | DOUBLE_BAR, sizeof(menu)-1);

	struct in_addr ip;
	ip.s_addr = q8_ip;
	y ++; clear_line(y, X_STS);
	wprintw(stdscr, "(A) Terminal IP Address = %s",
		ip.s_addr == 0 ? "DHCP/PXE" : inet_ntoa(ip));

	if (q8_ip != 0) {
		ip.s_addr = q8_srv0;
		y ++; clear_line(y, X_STS);
		wprintw(stdscr, "(B) Primary Q8 Server IP Address = %s", inet_ntoa(ip));

		ip.s_addr = q8_srv1;
		y ++; clear_line(y, X_STS);
		wprintw(stdscr, "(C) Secondary Q8 Server IP Address = %s", inet_ntoa(ip));

		ip.s_addr = q8_gw;
		y ++; clear_line(y, X_STS);
		wprintw(stdscr, "(D) Router IP Address = %s", inet_ntoa(ip));


		if (q8_nm_pwd.prefix == 0)
			q8_nm_pwd.prefix = 24;
		ip.s_addr = prefix_to_addr(q8_nm_pwd.prefix);
		
		y ++; clear_line(y, X_STS);
		wprintw(stdscr, "(E) Subnet Mask = %s", inet_ntoa(ip));
	}

	y ++; clear_line(y, X_STS);
	wprintw(stdscr, "(F) Password Status = %s", q8_nm_pwd.password ? "Enabled" : "Disabled");
	//wprintw(stdscr, "(F) Password = %s", q8_pwd);

	y ++; clear_line(y, X_STS);
	wprintw(stdscr, "(G) Load Default Values");

	y ++; clear_line(y, X_STS);
	wprintw(stdscr, "(H) Help");

	y ++; clear_line(y, X_STS);
	y ++; clear_line(y, X_STS);
	wprintw(stdscr, "(Q) Abort Change and Exit");
	y ++; clear_line(y, X_STS);
	wprintw(stdscr, "(S) Save Change and Exit");

	y ++; clear_line(y, X_STS);
	y ++; clear_line(y, X_STS);
	wprintw(stdscr, "Select Letter:");

	event_t evt = E_REDRAW;
	curs_set (1);
	
	bool go_loop_out = false;
	while (! go_loop_out) {
		go_loop_out = true;
		int key = getkey(0);
		switch (key) {
		case 'a':
		case 'A':
			q8_ip   = config_ip(stdscr, y, "Enter IP Address of this Terminal");
			break;
		case 'b':
		case 'B':
			q8_srv0 = config_ip(stdscr, y, "Enter IP Address of Q8 Primaray Server");
			break;
		case 'c':
		case 'C':
			q8_srv1 = config_ip(stdscr, y, "Enter IP Address of Q8 Secondry Server");
			break;
		case 'd':
		case 'D':
			q8_gw = config_ip(stdscr, y, "Enter IP Address of Default Gateway");
			break;
		case 'e':
		case 'E':
			q8_nm_pwd.prefix = addr_to_prefix(config_ip(stdscr,  y, "Enter NetMask of this Terminal"));
			break;
		case 'f':
		case 'F':
			q8_nm_pwd.password = config_pwd(stdscr, y, "Enter new password:");
			break;
		case 'q':
		case 'Q':
		case 'x':
		// system("reboot");
			evt = E_TIMEOUT;
			break;
		case 's':
		case 'S':
			config_save();
			// system("reboot");
			evt = E_TIMEOUT;
			break;
		default:
			go_loop_out = false;
			break;
		}
	}
	curs_set(0);
	return evt;
}

static struct net_device *netdev_sel(int index)
{
	struct net_device *netdev;

	int i = 0;
	for_each_netdev (netdev) {
		if (i == index)
			return netdev;
		i ++;
	}

	return NULL;
}

extern void (*ext_job_progress)(unsigned int sts);

/*
gui - 80x25

  0         1         2         3         4         5         6        7         8
  01234567890123456789012345678901234567890123456789012345678901234567801234567890
00+------------------------------------------------------------------------------+
 1|                                                                              | <--- title0
 2|                                                                              | <--- title1
 3+------------------------------------------------------------------------------+
 4|                                                                              |
 5|  status :   xxxxxxxxxxxxxxxxxxxxxxxxxxxxx                                    |
 6|                                                                              |
 7|                                                                              |
 8|                                                                              |
 9|                                                                              |
 0| Terminal IP information                                                      |
  | -----------------------                                                      |
  | x                                                                            |
  | x                                                                            |
  | x                                                                            |
  | x                                                                            |
  | x                                                                            |
  |                                                                              |
  | +--------------------------------------------------------------------------+ |
  | |                                                                          | |
  | +--------------------------------------------------------------------------+ |
  +------------------------------------------------------------------------------+
*/
#include <usr/ifmgmt.h>
#include <usr/imgmgmt.h>
enum {
	IPXE_SERVER,
	Q8VISTA_SERVER
};
extern unsigned int Base_PB, Max_PB;
static int check_net_dev(int net_n)  //return avaiable net device
{
	//net_n = net_n;
//#if 0	
	//	 WINDOW *win = stdscr;	
	// unsigned int sts_y = 6;
	// int link_status;

	// wmove(win, sts_y, X_STS);
	struct net_device *ndev = netdev_sel(net_n);
	if (ndev == NULL) {
		//wprintw(win, "Status: ERROR No Support Network Device Detected");
		return E_ERROR;
	}
	
	int rc = ifopen (ndev);
	if (rc != 0) {
		//wprintw(win, "Status: ERROR Open Network device Failed");
		return E_ERROR;
	}
	else
		ifclose(ndev);
	

	// if ((link_status = netdev_link_ok (ndev)) == 0){
	if (netdev_link_ok (ndev) == 0){
		//wprintw(win, "Status: ERROR Network device Link down");
		return E_ERROR;
	}	
	// wmove(win, sts_y+1, X_STS);
	// wprintw(win, "Net Status:[%s] TX:%d TXE:%d RX:%d RXE:%d", 
	// 	link_status? "up" : "down",
	// 	ndev->tx_stats.good, ndev->tx_stats.bad,
	// 	ndev->rx_stats.good, ndev->rx_stats.bad);
	// ifclose(ndev);
//#endif	
	return E_NEXT;
}

static int boot_progress(int netdevn)
{
	WINDOW *win = stdscr;
	char cmd[50];
	char download_file[2][15] = {"kernel", "initrd.gz"};
	struct image *img;
	char buf[128], cmd_t[200], cmd_long[500], ip_a[20];
	// char str1[100];
	char server_flag;
    unsigned char str1_idx, str_len;
	unsigned int sts_y = 5;
	unsigned int ter_y = 8;
	bar_y = ter_y + 8 + 2;

	wmove(win, sts_y, X_STS);

	struct net_device *ndev = netdev_sel(netdevn);
	if (ndev == NULL) {
		wprintw(win, "Status: ERROR No Support Network Device[%d] Detected", netdevn);
		return E_ERROR;
	}

	struct settings *parent = netdev_settings (ndev);
	struct in_addr ip, gw, nm, srv, srv2;

	int rc = ifopen (ndev);
	if (rc != 0) {
		wprintw(win, "Status: ERROR Open Network device[%d] Failed", netdevn);
		return E_ERROR;
	}

	if (q8_ip) {
		ip.s_addr = q8_ip;
		gw.s_addr = q8_gw;
		nm.s_addr = prefix_to_addr(q8_nm_pwd.prefix);
		store_setting(parent, &ip_setting,      &ip, sizeof(ip));
		store_setting(parent, &gateway_setting, &gw, sizeof(gw));
		store_setting(parent, &netmask_setting, &nm, sizeof(nm));		
	} else {
		rc = ifconf(ndev, NULL);
		if (rc != 0) {
			wprintw(win, "Status: ERROR Config Network device[%d] Failed", netdevn);
			return E_ERROR;
		}
	}

	const char info[] = "Terminal IP information";
	const char model[] = MODLE_NAME;

	wmove(win, ter_y,   X_STS);
	//wprintw(win, "%s [%d]", info, netdevn);
	wprintw(win, "%s", info);
	wmove(win, ++ter_y, X_STS);
	whline(win, win->attrs | SINGLE_BAR, sizeof(info)-1);

	fetch_ipv4_setting(parent, &ip_setting,      &ip);
	fetch_ipv4_setting(parent, &gateway_setting, &gw);
	fetch_ipv4_setting(parent, &netmask_setting, &nm);
	wmove(win, ++ter_y, X_STS); wprintw(win, "Model................... %s", model);
	wmove(win, ++ter_y, X_STS); wprintw(win, "IP Method .............. %s", q8_ip ? "STATIC" : "DHCP/PXE");
	wmove(win, ++ter_y, X_STS); wprintw(win, "Terminal IP............. %s", inet_ntoa(ip));

	char *raw_filename = NULL;
	char *p;
	if (q8_srv0 == 0) {
		fetch_ipv4_setting(parent, &dhcp_server_setting, &srv);
		wmove(win, ++ter_y, X_STS); wprintw(win, "Boot Server............. %s", inet_ntoa(srv));
		fetch_string_setting_copy (parent, &filename_setting, &raw_filename );
		if (raw_filename != NULL) {
			p = raw_filename;
			str1_idx = 0;
			str_len = 0;
			while(1)
			{
				if ((*p == 0x0) || (str_len > 19)) break;
				if (*p == '/')
					str1_idx = 0;
				else
				{
					buf[str1_idx++] = *p;
					p++;
				}
				str_len++;
			}
			buf[str1_idx]= 0x0;
			wmove(win, ++ter_y, X_STS); wprintw(win, "Boot file............... %s:%d", buf, str1_idx);
		}
	} else {
		srv.s_addr = q8_srv0;
		wmove(win, ++ter_y, X_STS); wprintw(win, "Primary Q8 Server....... %s", inet_ntoa(srv));
		srv2.s_addr = q8_srv1;
		wmove(win, ++ter_y, X_STS); wprintw(win, "Secondary Q8 Server..... %s", inet_ntoa(srv2));	
        store_setting(parent, &next_server_setting,  &srv, sizeof(srv)); 
	}
	wmove(win, ++ter_y, X_STS); wprintw(win, "Router.................. %s", inet_ntoa(gw));
	wmove(win, ++ter_y, X_STS); wprintw(win, "Subnet Mask............. %s", inet_ntoa(nm));
	wmove(win, ++ter_y, X_STS); wprintw(win, "MAC Address............. %s", netdev_addr(ndev));
    
	//check the server information
    //sprintf(buf, "tftp://%s/tftp://%s/%s/chainloader.bin", inet_ntoa(srv), inet_ntoa(srv), netdev_addr(ndev));
	//rc = imgcheck(buf, 0, &img);
    //if (rc != 0) {
        // wmove(win, 6, X_STS);
        // wprintw(win, "This is IPXE System");
    //    server_flag = IPXE_SERVER;
    //} else {
        // wmove(win, 6, X_STS);
        // wprintw(win, "This is Q8Vista System");	
		server_flag = Q8VISTA_SERVER;
	//}
    ext_job_progress = update_progress;
    update_progress(0);

    if (server_flag == Q8VISTA_SERVER)
	{
		Base_PB = 0;
		Max_PB = 30;
		const char *macstr;
		char mac_adr[20];
		int j;
		macstr = netdev_addr(ndev);
		mac_adr[0] = '0';mac_adr[1] = '1';mac_adr[2] = '-';
		for (j=3; *macstr !='\0'; macstr++, j++)
		{
		    mac_adr[j] = (*macstr ==':') ?'-' :*macstr;
		}
		mac_adr[j]='\0';
		sprintf(ip_a,"%s", inet_ntoa(ip));
		sprintf(cmd_t, "kernel tftp://%s/%s/%s ip=%s:%s:0.0.0.0:0.0.0.0 BOOTIF=%s IPMethod=%s", inet_ntoa(srv), macstr, download_file[0], ip_a, inet_ntoa(srv), mac_adr, q8_ip ? "STATIC" : "DHCP/PXE");
		sprintf(cmd_long, "%s CPU=6PVXL SYSVENDOR=Default string SYSPRODUCT=Default string SYSVERSION=Default string SYSSERIAL=Default string SYSSKU=default string SYSFAMILY=Default string MBVENDOR=Arista MBPRODUCT=B059 MBVERSION=V1.0 MBSERAL=Default string MBASSET=Default string BIOSVENDOR=American Megatrends Inc. BIOSVERSION=5.6.5 SYSFF=3 vga=788 splash=quiet bootsplash.bootfile=bootsplash/q8os quiet", cmd_t);

		wmove(win, 5, X_STS); wprintw(win, "status : %s                     ", download_file[0]);
		rc = system(cmd_long);
        if (rc != 0) {
            wmove(win, 5, X_STS);
            wprintw(win, "Status: ERROR download cfg failed");
            return E_ERROR;
        }
		Base_PB = 30;
		Max_PB = 70;
		sprintf(cmd, "initrd tftp://%s/%s", inet_ntoa(srv), download_file[1]);
		wmove(win, 5, X_STS); wprintw(win, "status : %s                      ", download_file[1]);
		rc = system(cmd);
        if (rc != 0) {
            wmove(win, 5, X_STS);
            wprintw(win, "Status: ERROR download cfg failed");
            return E_ERROR;
        }		
		Base_PB = 70;
		Max_PB = 90;
		wmove(win, 5, X_STS); wprintw(win, "status : Boot...                 ");
		update_progress(100);
		rc = system("boot");
        if (rc != 0) {
            wmove(win, 5, X_STS);
            wprintw(win, "Status: Boot failed");
            return E_ERROR;
        }		
		return 0;
	} else //(server_flag == IPXE_SERVER)
	{
		sprintf(buf, "tftp://%s/acpboot.bin", inet_ntoa(srv));
		rc = imgacquire(buf, 0, &img);
	    if (rc != 0) {
            wmove(win, sts_y, X_STS);
       	    wprintw(win, "Status: ERROR download cfg failed");
           	return E_ERROR;
	    }
	   	image_get(img);		
		image_exec(img);
	   	image_put(img);
	}

	return 0;
}

typedef enum {
	INIT,
	IDLE,
	WAIT_FOR_KEY,
	CONFIG_MODE,
	BOOT_MODE,

	BOOT_DHCP,
	BOOT_TFTP,

	BOOT_IFOPEN,
	BOOT_CFGDATA,
	BOOT_KERNEL,
	BOOT_INITRD,

	BOOT_EXEC,

} gui_state_t;
#define  NOLINKDEV   9999
static gui_state_t state = INIT;
static int main_loop (void)
{
	char str[50];
	WINDOW *win = stdscr;	
	event_t e_pending = E_REDRAW;    
	// unsigned int sts_y = 21;
	int tmo = 0;
	int idle_wait_key = 0;
	int net_dev_no = 0;
	int link_dev_no = 0;
	while (1) {
        // wmove(win, sts_y, X_STS);
       	// wprintw(win, "Status: %d,  e_pending: %d", state, e_pending);		
		if (e_pending) {
			switch (state) {
			case INIT:
				initscr();
				start_color();
				color_set ( CPAIR_NORMAL, NULL );  // set FG: wiite, BG = blue						
				curs_set ( 0 );		// the cursor state is set to invisible
				erase();
				draw_root_window();
				state = IDLE;
				idle_wait_key = 0;
				net_dev_no = 0;
				link_dev_no = NOLINKDEV;
				break;
			case IDLE:
			    if (idle_wait_key == 0)
				{
				    //sprintf(str, "Press any key for Configure IP[%d] Settings", net_dev_no);
					sprintf(str, "Press any key for Configure IP Settings");
					update_status("%s", str);
					idle_wait_key = 3;
				}
				if ((check_net_dev(net_dev_no) != E_ERROR) || (e_pending == E_KEY))  //check net dev connected
				{
					if (check_net_dev(net_dev_no) != E_ERROR)
						link_dev_no = net_dev_no;						
				    state = WAIT_FOR_KEY;
				    e_pending = 0;
					//clear_line(1, X_STS);
					//clear_line(2, X_STS);
					//wmove(win, 5, X_STS); 
					//wprintw(win, "Press any key for Configure IP[%d] Settings", net_dev_no);
					//wprintw(win, "Press any key for Configure IP Settings");
					tmo = 3;									
				}
				else if (e_pending != E_KEY)
					net_dev_no ^= 1;
				break;
			case WAIT_FOR_KEY:
			    net_dev_no = link_dev_no;
				if ((e_pending == E_TIMEOUT) && (link_dev_no != NOLINKDEV)) {  //if get file then boot:todo
					state = BOOT_MODE;
					clear_line(0, X_STS);
				} else if (e_pending == E_KEY) {
					if (q8_nm_pwd.password)
						check_password(win, 6);
					state = CONFIG_MODE;
				}
				e_pending = E_REDRAW;
				break;
			case CONFIG_MODE:
				if (e_pending == E_REDRAW)
					e_pending = update_config_menu();

                //exit config to BOOT MODE
				if (e_pending == E_TIMEOUT) {
					state = BOOT_MODE;
					e_pending = E_REDRAW;
					erase();
					draw_root_window();
					Base_PB = 0;  //reset progress bar
					Max_PB = 0;
				} 
				break;
			case BOOT_MODE:
				curs_set ( 0 );
				if (e_pending == E_REDRAW)
				{
					e_pending = boot_progress(link_dev_no);
					if (e_pending == E_ERROR)
					{
						state = INIT;
						net_dev_no ^= 1; //to another eno port
						tmo = 2;
					}
				}					
				else
					e_pending = 0;
				break;
			default:
				e_pending = 0;
				break;
			}
		}

		// if (e_pending == E_SHELL) {
		// 	/* dbg mode */
		// 	break;
		// }

		int key = 0;
		switch (state) {
		case IDLE:	
		case WAIT_FOR_KEY:
			key = getkey(1000); /* 1s */
			if (key == -1) {
				tmo --;
				
				if (tmo == 0)
					e_pending = E_TIMEOUT;

				if (idle_wait_key > 0)
					idle_wait_key--;
				else	
					idle_wait_key = 0;
			} else {
				e_pending = E_KEY;
			}
			break;
		default:
			key = getkey(100);
			if (key == CTRL_R) {
				system("reboot");
			} else if (key == CTRL_S) {
				system("shell");
			}
			break;
		}
		// wmove(win, sts_y+1, X_STS);
		// wprintw(win, "Statu2: %d,  e_pendin2: %d", state, e_pending);
	}

	return 0;
}



int q8_ui(void)
{
	int rc;

	config_load();
	getkey(100);
	rc = main_loop ();

	/* never reach here */
	//endwin();
	return rc;
}
